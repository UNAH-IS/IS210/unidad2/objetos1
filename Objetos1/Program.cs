﻿using System;

namespace Matricula
{
    class Program
    {
        static void Main(string[] args)
        {
            Alumno alumno = new Alumno("Jose", "20181001234", "Ing. Sistemas");

            alumno.SetNombre("Rafael");
            alumno.NumeroCuenta = "20025";
            alumno.Carrera = "Medicina";

            Alumno alumno2 = new Alumno("Marta", "20171001234", "Ing. Electrica");

            Console.WriteLine(alumno);
            Console.WriteLine(alumno2);

            
            Console.WriteLine("UNAH Privatizada");
            Alumno.SetUniversidad("UTH");

            Console.WriteLine(alumno);
            Console.WriteLine(alumno2);

            Console.ReadKey();
        }
    }
}
