﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matricula
{
    class Alumno
    {
        // Atributos
        private string nombre;
        string numeroCuenta;
        int indiceAnterior;
        string carrera;
        string contraseña;
        bool pagoMatricula;
        string centroEstudios;
        List<string> asignaturas;

        // Atributo estatico: Compartido entre todos los objetos de tipo Alumno
        static string Universidad = "UNAH";

        enum categorias
        {
            PrimerIngreso, Prosene, Deportes, Cultural, ExcelenciaAcademica,
            PorEgresar, PorIndice
        }

        // Metodos (Funciones)

        // Constructor
        public Alumno(string nombre, string numeroCuenta, string carrera)
        {
            this.nombre = nombre;
            this.numeroCuenta = numeroCuenta;
            this.carrera = carrera;
            this.asignaturas = new List<string>();
        }

        // Metodo get (lectura)
        public string GetNombre()
        {
            return this.nombre;
        }

        // Metodo set (escritura)
        public void SetNombre(string nombre)
        {
            this.nombre = nombre;
        }

        // Metodo ToString()
        public override string ToString()
        {
            return $"Nombre: {this.nombre}\nCuenta: {this.numeroCuenta}\nCarrera: {this.carrera}\nUniversidad: {Alumno.Universidad}\n";
        }

        public string mostrar()
        {
            return $"Nombre: {this.nombre}\nCuenta: {this.numeroCuenta}\nCarrera: {this.carrera}\nUniversidad: {Alumno.Universidad}\n";
        }

        // Propiedad de atributo
        public string NumeroCuenta
        {
            get
            {
                return this.numeroCuenta;
            }

            set
            {

                this.numeroCuenta = value;
            }
        }

        // Propiedades autoimplementadas. Estas no utilizan un atributo 
        public string Carrera
        {
            get;
            set;
        }

        // En c# es opcional colocar static en funciones que acceden a miembros (atributos) estaticos.
        // Sin embargo, se aconseja su uso.
        static public string GetUniversidad()
        {
            return Alumno.Universidad;
        }

        static public void SetUniversidad(string universidad)
        { 
            Alumno.Universidad = universidad;
        }
    }
}
